/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.DriveTrain.MotorType;

/**
 * Add your docs here.
 */
public class RobotConstants {

    // *******  PWM Ports *******
    // DriveTrain
	public static final int RIGHT_DEV_DRIVE_PWM = 0;
    public static final int LEFT_DEV_DRIVE_PWM  = 1;
    
    public static final int BLINKIN_PWM_CHANNEL = 9;
    
    //******* Analog Inputs *******
    public static final int SONAR_ANALOG_CHANNEL = 3;

    //******* Other Constants *******
    // Sonar color distances... 
	public static final double SONAR_GREEN_DIST     = 25;
	public static final double SONAR_YELLOW_DIST    = 10;
    
    // CAN IDs
    public static final int LEFT_LEAD_MOTOR_CAN_ID      = 10;
	public static final int LEFT_FOLLOW_MOTOR_CAN_ID    = 11;
	public static final int RIGHT_LEAD_MOTOR_CAN_ID     = 13;
	public static final int RIGHT_FOLLOW_MOTOR_CAN_ID   = 14;
    
    // Joystick ports
    public static final int DRIVER_JOYSTICK_PORT = 0;
    public static final int OPERATOR_JOYSTICK_PORT = 1;
	public static final MotorType DEFAULT_DRIVETRAIN_CONTROLLER_TYPE = DriveTrain.MotorType.Victor;

}
